## Memex website

The below is a copy of the website was set up from a clone of the html repo from GitLab
This repo was then **set to public** and all files relating to the simple static html website that it Memex have been uploaded

Note: as cloudflare is used to manage DNS records there can be a loop caused when it comes to SSL and this can cuase the website not to load.
To resolve this Verify your SSL/TLS configuration in Cloudflare. If you have Cloudflare's SSL/TLS encryption mode set to "Flexible", it could cause redirect loops because Cloudflare tries to connect to your server over HTTP, but your server (or GitLab Pages) redirects it back to HTTPS. Changing the encryption mode to "Full" (if your server supports HTTPS) or "Full (strict)" (if you have a valid SSL certificate installed on your server) might solve the problem.

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yml
image: busybox

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/solutions/continuous-integration/
[index.html]: https://gitlab.com/pages/plain-html/-/blob/main/public/index.html
[userpages]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-pages-in-projects-and-groups
[projpages]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-pages-in-projects-and-groups
